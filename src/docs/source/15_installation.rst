Setup
=====


Pre-requisites
--------------

*   Hitachi Content Platform (HCP)

*   A Tenant-level user account having read/write/delete access to an HCP
    Namespace



Dependencies
------------

You need to have at least Python 3.4.3 installed to run **hcpt**.

It depends on the `hcpsdk <http://hcpsdk.readthedocs.org/en/latest/>`_ to
access HCP.

You might want to use a virtual environment to fence the dependency
from your primary Python environment...


Installation
------------

**Make sure you have Python 3.4.3 (or better) installed**

In case it's not installed, get it
`here <https://www.python.org/downloads/>`_.

There are two ways to install **hcpt**:

1.  system-wide

    *   Install **hcpt** by running:

        ..  code-block:: text
            :emphasize-lines: 1

            $ pip install hcpt

    -or-

    *   Get the source from
        `gitlab.com <https://gitlab.com/simont3/hcptool>`_  either

        *   by downloading the source archive, or

        *   by cloning the repository:

            ..  code-block:: text
                :emphasize-lines: 1

                $ git clone https://gitlab.com/simont3/hcptool


    *   Install locally, including the dependency:

        ..  code-block:: text
            :emphasize-lines: 1

            $ python setup.py install


2.   in a virtual environment

    **WINDOWS**

    *   Create a fresh virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            C:\>\Python35\Tools\scripts\pyvenv.py c:\temp\_venv_hcpt

    *   Activate the virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1,3

            C:\>cd temp

            C:\temp>\temp\_venv_hcpt\Scripts\activate.bat
            (_venv_hcpt) C:\temp>

    *   Install **hcpt**:

        ..  code-block:: text
            :emphasize-lines: 1

            (_venv_hcpt) C:\temp>pip install hcpt
            Collecting hcpt
              Downloading hcpt-1.0.19.tar.gz
            Collecting hcpsdk>=0.9.3.post0 (from hcpt)
              Downloading hcpsdk-0.9.3.post4.tar.gz
            Collecting dnspython3==1.12.0 (from hcpsdk>=0.9.3.post0->hcpt)
              Downloading dnspython3-1.12.0.zip (226kB)
                100% |################################| 229kB 1.3MB/s
            Collecting alabaster>=0.7.1 (from hcpsdk>=0.9.3.post0->hcpt)
              Downloading alabaster-0.7.6.tar.gz
            Installing collected packages: dnspython3, alabaster, hcpsdk, hcpt
              Running setup.py install for dnspython3
              Running setup.py install for alabaster
              Running setup.py install for hcpsdk
              Running setup.py install for hcpt
            Successfully installed alabaster-0.7.6 dnspython3-1.12.0 hcpt-1.0.19 hcpsdk-0.9.3.post4

    Now you can run **hcpt** as long as you have the virtual environment
    activated:

    ..  code-block:: text
        :emphasize-lines: 1

        C:\temp>hcpt
        blablabla

    **Linux**

    *   Create a fresh virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            $ pyvenv _venv_hcpt

    *   Activate the virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            $ source _venv_hcpt/bin/activate


    *   Install **hcpt**:

        ..  code-block:: text
            :emphasize-lines: 1

            (_venv_hcpt) $ pip install hcpt
            Collecting hcpt
              Downloading hcpt-1.0.19.tar.gz
            Collecting hcpsdk>=0.9.3.post0 (from hcpt)
              Downloading hcpsdk-0.9.3.post4.tar.gz
            Collecting dnspython3==1.12.0 (from hcpsdk>=0.9.3.post0->hcpt)
              Downloading dnspython3-1.12.0.zip (226kB)
                100% |################################| 229kB 7.4MB/s
            Collecting alabaster>=0.7.1 (from hcpsdk>=0.9.3.post0->hcpt)
              Downloading alabaster-0.7.6.tar.gz
            Installing collected packages: dnspython3, alabaster, hcpsdk, hcpt
              Running setup.py install for dnspython3
              Running setup.py install for alabaster
              Running setup.py install for hcpsdk
              Running setup.py install for hcpt
            Successfully installed alabaster-0.7.6 dnspython3-1.12.0 hcpt-1.0.19 hcpsdk-0.9.3.post4

    Now you can run **hcpt** as long as you have the virtual environment
    activated:

    ..  code-block:: text
        :emphasize-lines: 1

        (_venv_hcpt) $ hcpt
        blablabla


Verification
------------

*   Make sure that you have access to an HCP system and valid user credentials
    for an authenticated namespace at hand. The test may also be run against
    the default namespace, where’s no need to have user credentials (simply
    provide random user / password).

*   Get a command prompt

*   If ever possible, switch of the local machine’s DNS cache::

          c:> net stop dnscache

*   Run **hcpt** in test mode::

        c:> hcpt -i5 --user <user> --password <password> test --cluster \
                  ns.tenant.hcp.vm.loc --dir /rest/hcpt_test --file <filename>
                  --structure 10 100

    (if the target namespace has ‘versioning’ eabled, add --versionedNS to the
    command above!)

**hcpt** will...


1.  calculate access tokens for both namespace and
2.  MAPI access
3.  feed file <filename> 100 times in each of 10 subdirectories of
    ``/rest/hcpt_test`` within namespace ‘ns’ in tenant ‘tenant’ within an HCP
    with a DNS name of ‘hcp.vm.loc’
4.  discover a list of all objects and directories, beginning at
    ``/rest/hcpt_test``. This list will be provided as 2 files: hcpt_test.csv
    (to be loaded into MS Excel) and hcpt_list.<timestamp>.db, a
    Sqlite3-database that will be used as base for the nexts steps.
5.  update the database file with a new retention setting to be processed in
    the next step.
6.  update HCP with the new retention settings for the objects selected in the
    database in the prior step.
7.  delete (purge) all the objects and the directories where they have been
    stored.

At the end, there will be a logfile (hcp_test.log) showing all the output that
has been showed on the screen, an Excel-importable file (hcp_test.csv)
containing a list of all objects and directories found in 4. and two Sqlite3
database files, containing the results of 4. and 7.


**test** run output:

.. include:: _static/testrun.rst