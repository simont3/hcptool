HCP Tools (|release|)
=====================

HCP Tool (**hcpt**) offers some functions that might be useful when
working with Hitachi Content Platform (HCP):

    *   Calculate the access token needed to access an authenticated namespace or the Management API
    *   Load HCP with test data
    *   List the content of a namespace (or parts of it)
    *   Change the retention of objects within HCP
    *   Delete objects from HCP, supporting Purge and Privileged Delete

..  Warning::

    Some commands may have severe impact on a production HCP:

    **load** - if you load data into a namespace running in ‚compliance‘ mode
    while setting a retention for the ingested objects, you will not be able to
    delete these objects before the retention has expired!

    **retention** - you might lock data in longer retention then wanted if you
    use an incorrect retention string - please refer to ‚Using a Namespace‘ on
    how to form this string!

    **unload** - you might delete (purge) objects under retention in a
    namespace in ‚enterprise‘ mode.


..  toctree::
    :maxdepth: 3

    15_installation
    20_usage
    50_errors
    70_howto
    97_changelog
    98_license
    99_about

..
    Indices and tables
    ==================

    *   :ref:`genindex`
    *   :ref:`search`

