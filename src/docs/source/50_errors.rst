Error Handling
==============

.. _winerror10048:

**Windows "10048 - Address already in use" errors**

If **hcpt** is used effectively, it may send many requests per second to
HCP. This may lead to *10048 - Address already in use* errors. This happens
because Windows doesn’t release used (and closed) TCP/IP-scockets until a
timeout of 240 seconds (default) has elapsed. This adds to the fact, that the
nummer of outgoing TCP/IP-ports is limited to less than 4.000. Taken as fact
that every socket needs it’s own outgoing port, it get’s clear that we will run
out of free sockets when doing multi-ten requests per second. **hcpt**
catches the described error, waits a second and then tries again - but that’s
just a workaround slowing down the process substantially.

..  Tip::

    The **load** and **list** functions are already using the
    `hcpsdk <http://hcpsdk.readthedocs.org/en/latest/>`_.
    The fix described below isn't required if using these functions, only!

This can be fixed by:

*   Lowering the „TCP/IP socket connection timeout“ from 240 seconds (default)
    to e.g. 30 seconds. A Registry-Key needs to be added as DWORD::

        HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\TcpTimedWaitDelay = 30

    In the test environment, there was a gain of 20% in performance; the error
    disappeared.

*   Increasing the number of dynamic available TCP/IP Ports (indirect by
    increasing the highst available dynamic Port). A Registry-Key needs to be
    added as DWORD::

        HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\MaxUserPort = 32768

    This seems to put more load on the systems – CPU-load is substancially
    higher and the system feels tenacious.

Please consider:

*   Changing these Registry-Keys needs a system restart to activate them!
*   Changing theses keys has effect for the whole system - other applications
    could be affected!

These Changes must be done with care - the author is not willing to take
responsibility for whatever impact they might have!

See also: http://msdn.microsoft.com/en-us/library/aa560610%28v=bts.20%29.aspx