License / Trademarks
====================

The MIT License (MIT)
---------------------

Copyright (c) 2011-2016 Thorsten Simons (sw@snomis.de)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.


Trademarks and Copyrights of used material
------------------------------------------

Hitachi is a registered trademark of Hitachi, Ltd., in the United States and
other countries. Hitachi Data Systems is a registered trademark and service
mark of Hitachi, Ltd., in the United States and other countries.

Archivas, Hitachi Content Platform, Hitachi Content Platform Anywhere
and Hitachi Data Ingestor are registered trademarks of Hitachi Data Systems
Corporation.

All other trademarks, service marks, and company names in this document or web
site are properties of their respective owners.

The logo used in the documentation is based on the picture found at
`pixabay.com <https://pixabay.com/en/toolbox-tool-box-hammer-tool-box-807845//>`_,
where it is declared under
`CC0 Public Domain <https://pixabay.com/service/terms/#usage>`_ license.

