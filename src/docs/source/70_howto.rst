HowTo
=====

**Compare the content of two namespaces (or two directory trees)?**

    Generate a list of all objects for each of the namespaces (or directory
    trees)::

        hcpt --user ns1 --password ns101 -v -l hcp1.log -i3 \
             list --cluster ns1.matrix.hcp1.vm.local --dir /rest \
             --database hcp1_db --keepDB --fatDB --nooutfile
        hcpt --user ns1 --password ns101 -v -l hcp2.log -i3 \
             list --cluster  ns1.matrix.hcp2.vm.local --dir /rest \
             --database hcp2_db --keepDB --fatDB --nooutfile

    You’ll end up with two files, ``hcp1_db`` and ``hcp2_db``, each containing
    a SQLite3 database. You’ll also have two logfiles, ``hcp1.log`` and
    ``hcp2.log``, at whose end you’ll find the number of objects found.

    Now, prepare a command file ``cmd.sqlite`` with this content::

        attach database hcp2_db as hcp2;
        .header on
        .output compare.txt
        select min(urlname) from
        (Select urlname, size, retentionstring
         from main.flist
         Union all
         Select urlname, size, retentionstring
         from hcp2.flist
        ) tmp
        group by urlname
        having count(*) = 1
        order by urlname;

    Run ``sqlite3`` to find the differences between the namespaces (or
    directory trees)::

        sqlite3 -init cmd.sqlite hcp1_db

    You’ll end up with a file ``compare.txt``, holding the paths/names of those
    objects available in only one namespace (or directory tree).
