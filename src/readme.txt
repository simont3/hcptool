This software is released under the MIT License - please refer to License.txt.

Hitachi Content Platform (HCP) is a Content Store (and/or archival) platform
sold by Hitachi Data Systems (www.hds.com). 

HCP Tool is a set of tools that might help when working with HCP.
  - Calculate the access token needed to access an authenticated namespace or
    the Management API
  - Load HCP with test data
  - List the content of a namespace (or parts of it) 
  - Change the retention of objects within HCP 
  - Delete objects from HCP, supporting Purge and Priviledged Delete

Based on Python 3.2.2, you have several choices on how to install it:

1. install it into you Python's site-packages folder:
	- python setup.py install

2. Make a Windows executable:
	- python setupexe.py build
	  (you'll need to have cx_freeze (http://cx-freeze.sourceforge.net/)
	  installed)
	- use a Setup compiler to build a Windows installer
	  (a script for Inno Setup (http://www.innosetup.com) is provided in
	  subfolder InnoSetup)
