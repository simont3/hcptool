'''
The MIT License (MIT)

Copyright (c) 2012-2018 Thorsten Simons (sw@snomis.de)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''
from hcptcmds.init import Gvars

outFile = '../PKGBUILD'

a = []
a.append("# Maintainer: {}".format(Gvars.Author))
a.append("")
a.append("pkgname={}".format(Gvars.s_description))
a.append("pkgver={}".format(Gvars.s_version))
a.append("pkgrel={}".format('1'))
a.append("pkgdesc='{}'".format(Gvars.Description))
a.append("arch=('x86_64')")
a.append("license=('custom')")
a.append("depends=(python xz sqlite)")
a.append("options=(!strip)")
a.append("changelog='src/history.txt'")
a.append("prepare() {")
# a.append("    echo 'Compiling help site...'")
# a.append("    python3 genhelp.py")
# a.append("    python3 genusrdoc.py")
a.append("    echo 'Freezing $pkgname...'")
a.append("    python3 setuplx.py bdist")
a.append("}")
a.append("package() {")
a.append("    cd $pkgdir")
a.append("    tar -xzf $srcdir/dist/$pkgname-$pkgver.linux-x86_64.tar.gz")
# a.append("    cp $srcdir/hcpm-loop usr/bin")
# a.append("    chmod 755 usr/bin/hcpm-loop")
# a.append("    mkdir -p usr/share/licenses/$pkgname")
# a.append("    mv usr/lib/Documentation/license.txt usr/share/licenses/$pkgname/LICENSE")
# a.append("    mkdir -p usr/share/man/man1")
# a.append("    cp $srcdir/help/_build/man/hcpm.1 usr/share/man/man1")
# a.append("    cp $srcdir/help/_build/man/hcpmctl.1 usr/share/man/man1")
# a.append("    cp $srcdir/help/_build/man/rbformat_check.1 usr/share/man/man1")
# a.append("    chmod 644 usr/share/man/man1/hcpm.1")
# a.append("    chmod 644 usr/share/man/man1/hcpmctl.1")
# a.append("    chmod 644 usr/share/man/man1/rbformat_check.1")
# a.append("    mkdir -p srv/http/hcpm")
# a.append("    cd srv/http/hcpm")
# a.append("    cp -R $srcdir/help/_build/html/* .")
# a.append("    cp $srcdir/help/_build/latex/hcpm.pdf .")
# a.append("    cp $srcdir/usrdoku/_build/latex/HCP_migration_tool_2_Users_Manual.pdf .")
# a.append("    cd $pkgdir")
# a.append("    cp $srcdir/usrdoku/_build/latex/HCP_migration_tool_2_Users_Manual.pdf usr/lib/Documentation/HCP_migration_tool_2_Users_Manual.pdf")
a.append("    chown -R root usr")
a.append("    chgrp -R root usr")
# a.append("    chown -R root srv")
# a.append("    chgrp -R root srv")
a.append("}")


with open(outFile, 'w') as outHdl:
    print('Writing', outFile, '...')
    for i in a:
        print(i, file=outHdl)

print('Finished.')
