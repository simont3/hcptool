'''
The MIT License (MIT)

Copyright (c) 2012-2018 Thorsten Simons (sw@snomis.de)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

import tarfile
import os
import compileall
from hcptcmds.hcpinit import gvars

# 1nd, pre-compile all source files
compileall.compile_dir('.', legacy=True)

# 2rd, make an archive of all relevant files
excl = ['setup_linux.pyc', 'setup_win.pyc', 'export2linux2.pyc',
        'hcpf.cpython-32.pyc', 'hcpinit.cpython-32.pyc',
        '__init__.cpython-32.pyc', 'hcpcookie.cpython-32.pyc',
        'hcplist.cpython-32.pyc', 'hcpload.cpython-32.pyc',
        'hcpretention.cpython-32.pyc', 'hcptest.cpython-32.pyc',
        'hcpunload.cpython-32.pyc']
srcArchive = os.path.join('dist', 'hcpt_{0}.precomp.tar'.format(gvars._version))
tf = tarfile.open(srcArchive, 'w')
for root, dirs, files in os.walk(os.path.curdir):
    if files:
        for _file in files:
            if _file in excl:
                continue
            if os.path.splitext(_file)[1] == '.pyc' or \
               os.path.splitext(_file)[1] == '.txt' or \
               os.path.splitext(_file)[1] == '.pdf' or \
               os.path.splitext(_file)[1] == '.tar' or \
               os.path.splitext(_file)[1] == '.sh':
                tf.add(os.path.join(root,_file))
tf.close()
