'''
The MIT License (MIT)

Copyright (c) 2012-2018 Thorsten Simons (sw@snomis.de)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''
import time
import telnetlib

### START OF CONFIG AREA ###
HOST = "hcpm-devel2.snomis.local"
user = 'dev'
password = input('{}\'s password, please: '.format(user))
logfile = '_buildlinux.log'

cmds = [b'rm -Rf hcptool',
        b'git clone https://gitlab.com/simont3/hcptool.git',
        b'echo "now: cd hcptool/src"',
        b'cd hcptool/src',
        b'python3 genPKGBUILD.py',
        b'cd ..',
        b'makepkg',
        b'cp hcpt-*-x86_64.pkg.tar.xz ../dev',
        ]
##### END OF CONFIG AREA ###

def runBuild():
    with open(logfile, 'w') as log:
        tn = telnetlib.Telnet(HOST)
        tn.set_debuglevel(0)
        
        tn.read_until(b"login: ")
        tn.write(user.encode('ascii') + b"\n")
        if password:
            tn.read_until(b"Password: ")
            tn.write(password.encode('ascii') + b"\n")
        
        for i in cmds:
            ans = tn.read_until(br"]$ ").decode('utf8')
            for j in ans.splitlines():
                print('\n'+j, end='')
                print('\n'+j, file=log, end='')
            tn.write(i + b'\n')
            time.sleep(1)
        
        tn.write(b"exit\n")
        ans = tn.read_all().decode('utf8')
        print(ans)
        print(ans, file=log)



if __name__ == '__main__':
    runBuild()
